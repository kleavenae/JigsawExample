package hu.kleatech.jigsaw;

import hu.kleatech.jigsaw.api.Api;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
public class MainApplication implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Autowired private Api api;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Testing @Autowire on API...");
        api.test();
        System.out.println("Testing @Autowired on API which has @Autowired on an implementation of the Persistence interface...");
        api.testRepo();
    }
}
