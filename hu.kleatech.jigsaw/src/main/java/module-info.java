module hu.kleatech.jigsaw {
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires java.sql;
    exports hu.kleatech.jigsaw to spring.core, spring.beans, spring.context;
    opens hu.kleatech.jigsaw to spring.core;

    requires hu.kleatech.jigsaw.api;
    requires spring.beans;
}