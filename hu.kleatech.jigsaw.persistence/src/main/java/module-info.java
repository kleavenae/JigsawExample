module hu.kleatech.jigsaw.persistence {
    requires spring.context;

    exports hu.kleatech.jigsaw.persistence;
}